<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once dirname(__DIR__) . '/lotto-with-video/vendor/autoload.php';

if (isset($_GET['itr'])) {
    $itr = (int) $_GET['itr'];
} else {
    $itr = 0;
}


if (isset($_GET['video'])) {
    // if ($_SERVER['REMOTE_ADDR'] === '194.158.213.102' ||
    //     $_SERVER['REMOTE_ADDR'] === '116.203.212.197' ||
    //     $_SERVER['REMOTE_ADDR'] === '95.217.73.101' ||
    //     $_SERVER['REMOTE_ADDR'] === '127.0.0.1' ||
    //     $_SERVER['REMOTE_ADDR'] === '116.203.212.197'
    // ) {
        if ($_GET['video'] === 'fhvht7r2v873783') { //make2
            $videoMaker = new \App\VideoMakerWithoutFFMPEG($itr);
            $fd = fopen("logs.txt", 'a+') or die("не удалось открыть файл");
            fseek($fd, 0, SEEK_END);
            $str = date('h:i:s_m-d-y') . " ip: {$_SERVER['REMOTE_ADDR']}  \r\n";
            fwrite($fd, $str);
            fclose($fd);

            $status = $videoMaker->makeVideo();

            if ($status) {
                echo 'complete';
            } else {
                echo 'fail';
            }
        }

        if ($_GET['video'] === '8797598365734') { //make2
            $videoMaker = new \App\VideoMakerWithoutFFMPEG2($itr);
            $fd = fopen("logs.txt", 'a+') or die("не удалось открыть файл");
            fseek($fd, 0, SEEK_END);
            $str = date('h:i:s_m-d-y') . " ip: {$_SERVER['REMOTE_ADDR']}  \r\n";
            fwrite($fd, $str);
            fclose($fd);

            $status = $videoMaker->makeVideo();

            if ($status) {
                echo 'complete';
            } else {
                echo 'fail';
            }
        }

        if ($_GET['video'] === '12346352435') { //make2
            $videoMaker = new \App\VideoMakerWithoutFFMPEG3($itr);
            $fd = fopen("logs.txt", 'a+') or die("не удалось открыть файл");
            fseek($fd, 0, SEEK_END);
            $str = date('h:i:s_m-d-y') . " ip: {$_SERVER['REMOTE_ADDR']}  \r\n";
            fwrite($fd, $str);
            fclose($fd);

            $status = $videoMaker->makeVideo();

            if ($status) {
                echo 'complete';
            } else {
                echo 'fail';
            }
        }
    // }

    // if ($_GET['video'] === 'ffrecode') {
    //     $status = $videoMaker->ffRecodeVideo();
    // }
} elseif (isset($_GET['name'])) {
    $fd = fopen("name.txt", 'w+') or die("не удалось открыть файл");
    fwrite($fd, $_GET['name']);
    fclose($fd);
} else {
    http_response_code(404);
    echo 'page not found';
}
