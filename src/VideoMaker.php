<?php

namespace App;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverDimension;
use FFMpeg\FFMpeg;

class VideoMaker
{
    protected $workDirectoryPath = '/var/www/lotto-with-video/video/';

    protected $mode = 'full';

    protected $itr = 181;

    public function __construct($itr)
    {
        if ($itr !== 0) {
            $this->itr = $itr;
        }

    }

    public function makeVideo(): bool
    {
        $temporaryFiles = [];

        // Запись видео
        $filename = $this->startCreatingVideoOnSelenium();

        $temporaryFiles[] = $filename;
        // получение видео от селениума
        $this->copyVideoFromSelenium($filename, $this->workDirectoryPath);

        // Укорачивание продолжительности
        $filename = $this->shortenVideoDuration($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Обрезание части экрана
        $filename = $this->croppingPartOfScreen($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Переименование видео
        $this->renameVideo($filename, $this->workDirectoryPath);

        // Удаление промежуточных файлов
        $this->deleteIntermediateFiles($temporaryFiles, $this->workDirectoryPath);

        return true;
    }

    public function ffRecodeVideo(): bool
    {
        $filename = '44.mp4';
        $temporaryFiles[] = $filename;

        // получение видео от селениума
        $this->copyVideoFromSelenium($filename, $this->workDirectoryPath);

        // Укорачивание продолжительности
        $filename = $this->shortenVideoDuration($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Обрезание части экрана
        $filename = $this->croppingPartOfScreen($filename, $this->workDirectoryPath);
        $temporaryFiles[] = $filename;

        // Переименование видео
        $this->renameVideo($filename, $this->workDirectoryPath);

        // Удаление промежуточных файлов
        $this->deleteIntermediateFiles($temporaryFiles, $this->workDirectoryPath);

        return true;
    }

    protected function startCreatingVideoOnSelenium(): string
    {
        echo 'Start creating video on selenium...<br>';
        $filename = "44.mp4";
        $host = 'http://localhost:4444/wd/hub';
        $capabilities = [
            WebDriverCapabilityType::BROWSER_NAME => 'chrome',
            'enableVideo' => true,
            'screenResolution' => '2560x1440x24',
            'videoName' => $filename,
            'videoFrameRate' => 65
        ];
        $webDriver = RemoteWebDriver::create($host, $capabilities, 1200000, 1200000);
        $webDriver->manage()->window()->setSize(new WebDriverDimension(2560, 1440));

        $webDriver->get("http://95.217.73.101/game/index.html");
        //$webDriver->get("https://stikler51.github.io/lotto/");
        //$webDriver->get("https://sergeikurbako.github.io/lotto/");

        sleep(20);
        $webDriver->findElement(WebDriverBy::xpath('/html/body/button'))->click();
        sleep(10);
        $webDriver->findElement(WebDriverBy::xpath('/html/body/div/button'))->click();

        for ($i = 0; $i < $this->itr; $i++) {
            $webDriver->findElement(WebDriverBy::cssSelector('body'));
            sleep(15);
        }

        $webDriver->quit();

        // if ($this->mode === 'demo') {
        //     $checkShortenVideo = $this->checkShortenVideo($filename, $this->workDirectoryPath, 15);
        // } else {
        //     $checkShortenVideo = $this->checkShortenVideo($filename, $this->workDirectoryPath, 152423172);
        // }

        echo 'done<br>';
        return $filename;
    }

    protected function copyVideoFromSelenium(string $filename, string $workDirectoryPath): bool
    {
        echo 'Copy video from selenium...<br>';
        $url = 'http://localhost:4444/video/' . $filename;
        $path = $workDirectoryPath . $filename;
        file_put_contents($path, file_get_contents($url));
        echo 'done<br>';

        return true;
    }

    protected function shortenVideoDuration(string $filename, string $workDirectoryPath): string
    {
        $newFilename = 'cut.mp4';

        $this->removeIfFileExist($newFilename, $workDirectoryPath);

        echo 'Shorten video duration...<br>';
        if ($this->mode === 'demo') {
            \shell_exec("ffmpeg -i $workDirectoryPath$filename -ss 00:00:16 -t 00:05:00 -async 1 $workDirectoryPath$newFilename");
        } else {
            \shell_exec("ffmpeg -i $workDirectoryPath$filename -ss 00:00:16 -t 00:25:00 -async 1 $workDirectoryPath$newFilename");
        }
        echo 'done<br>';

        // проверка правильности обрезки видео
        // if ($this->mode === 'demo') {
        //     $checkShortenVideo = $this->checkShortenVideo($newFilename, $workDirectoryPath, 150);
        // } else {
        //     $checkShortenVideo = $this->checkShortenVideo($newFilename, $workDirectoryPath, 102423172);
        // }

        return $newFilename;
    }

    protected function croppingPartOfScreen(string $filename, string $workDirectoryPath): string
    {
        $newFilename = 'crop.mp4';

        $this->removeIfFileExist($newFilename, $workDirectoryPath);

        echo 'Cropping part of screen...<br>';
        \shell_exec("ffmpeg -y -hide_banner -i '$workDirectoryPath$filename' -filter:v 'crop=iw-0:ih-100,scale=2560:1340' -pix_fmt yuv420p $workDirectoryPath$newFilename");
        echo 'done<br>';

        return $newFilename;
    }

    protected function renameVideo(string $filename, string $workDirectoryPath): string
    {
        $serverDate = date('h:i:s_m-d-y');

        $newFilename = $serverDate . '.mp4';

        // проверка наличия файла копия которого будет сделана
        if (file_exists($workDirectoryPath . $filename)) {
            echo 'Rename video...<br>';
            copy($workDirectoryPath . $filename, $workDirectoryPath . $newFilename);
            echo 'done<br>';
        }

        $this->removeIfFileExist($filename, $workDirectoryPath);

        return $newFilename;
    }

    protected function deleteIntermediateFiles(array $temporaryFiles, string $workDirectoryPath): bool
    {
        echo 'Delete intermediate files...<br>';
        foreach ($temporaryFiles as $file) {
            $this->removeIfFileExist($file, $workDirectoryPath);
        }
        echo 'done<br>';

        return true;
    }

    // protected function checkShortenVideo(string $filename, string $workDirectoryPath, int $size = 152423172): bool
    // {
    //     if (file_exists($workDirectoryPath . $filename)) {
    //         $fileSize = filesize($workDirectoryPath . $filename);
    //
    //         if ($fileSize < $size) {
    //             if ($this->mode === 'demo') {
    //                 exit(__METHOD__ . '; $size = '. $size);
    //             }
    //             // удаление предыдущей попытки
    //             unlink($workDirectoryPath . $filename);
    //
    //             return false;
    //         }
    //     }
    //
    //     return true;
    // }

    protected function removeIfFileExist(string $filename, string $workDirectoryPath): bool
    {
        if (file_exists($workDirectoryPath . $filename)) {
            unlink($workDirectoryPath . $filename);
            return true;
        } else {
            return false;
        }
    }
}
