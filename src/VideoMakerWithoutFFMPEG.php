<?php

namespace App;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\WebDriverKeys;
use Facebook\WebDriver\WebDriverDimension;

class VideoMakerWithoutFFMPEG
{
    protected $workDirectoryPath = '/var/www/lotto-with-video/video/';

    protected $mode = 'full';

    protected $itr = 181;

    public function __construct($itr)
    {
        if ($itr !== 0) {
            $this->itr = $itr;
        }
    }

    public function makeVideo(): bool
    {
        // Запись видео
        $filename = $this->startCreatingVideoOnSelenium();

        // получение видео от селениума
        $this->copyVideoFromSelenium($filename, $this->workDirectoryPath);

        // Переименование видео
        $this->renameVideo('44.mp4', $this->workDirectoryPath);

        if (file_exists($this->workDirectoryPath . '44.mp4')) {
            unlink($this->workDirectoryPath . '44.mp4');
        }

        return true;
    }

    protected function startCreatingVideoOnSelenium(): string
    {
        echo 'Start creating video on selenium...<br>';
        $filename = "44.mp4";
        $host = 'http://localhost:4444/wd/hub';
        $capabilities = [
            WebDriverCapabilityType::BROWSER_NAME => 'chrome',
            'enableVideo' => true,
            'screenResolution' => '2560x1440x24',
            'videoName' => $filename,
            'videoFrameRate' => 65
        ];
        $webDriver = RemoteWebDriver::create($host, $capabilities, 1200000, 1200000);
        $webDriver->manage()->window()->setSize(new WebDriverDimension(2560, 1440));

        $webDriver->get("http://95.217.73.101/game/index.html");
        //$webDriver->get("https://stikler51.github.io/lotto/");
        //$webDriver->get("https://sergeikurbako.github.io/lotto/");

        sleep(20);
        $webDriver->findElement(WebDriverBy::xpath('/html/body/button'))->click();
        sleep(10);
        $webDriver->findElement(WebDriverBy::xpath('/html/body/div/button'))->click();

        for ($i = 0; $i < $this->itr; $i++) {
            $webDriver->findElement(WebDriverBy::cssSelector('body'));
            sleep(15);
        }

        $webDriver->quit();

        echo 'done<br>';
        return $filename;
    }

    protected function copyVideoFromSelenium(string $filename, string $workDirectoryPath): bool
    {
        echo 'Copy video from selenium...<br>';
        $url = 'http://localhost:4444/video/' . $filename;
        $path = $workDirectoryPath . $filename;
        file_put_contents($path, file_get_contents($url));
        echo 'done<br>';

        return true;
    }

    protected function renameVideo(string $filename, string $workDirectoryPath): string
    {
        $name = \file_get_contents('/var/www/lotto-with-video/name.txt');
        //$name = date('h:i:s_m-d-y');

        $newFilename = $name . '.mp4';

        // проверка наличия файла копия которого будет сделана
        if (file_exists($workDirectoryPath . $filename)) {
            echo 'Rename video...<br>';
            copy($workDirectoryPath . $filename, $workDirectoryPath . $newFilename);
            echo 'done<br>';
        }

        return $newFilename;
    }

}
