/*
  Zooming and rotating HTML5 video player
  Homepage: http://github.com/codepo8/rotatezoomHTML5video
  Copyright (c) 2011 Christian Heilmann
  Code licensed under the BSD License:
  http://wait-till-i.com/license.txt
*/

var zoom = 1, rotate = 0;

(function() {

    /* predefine zoom and rotate */


    /* Grab the necessary DOM elements */
    var stage = document.getElementById('stage'),
        v = document.getElementsByTagName('video')[0],
        v2 = document.getElementById('video2'),
        controls = document.getElementById('controls');

    // Выбор рандомного видео с номером от 1 до 30
    // Раскомментировать две строки ниже, прописать верный путь к папке с видео
    // Закомментировать строку 28
    // var rand = Math.floor(1 + Math.random() * 30);
    // vid.src = `video/output-${rand}.webm`;

    v2.src = 'https://stikler51.github.io/lotto/video/output.webm';


    /* Array of possible browser specific settings for transformation */
    var properties = ['transform', 'WebkitTransform', 'MozTransform',
            'msTransform', 'OTransform'
        ],
        prop = properties[0];

    /* Iterators and stuff */
    var i, j, t;

    /* Find out which CSS transform the browser supports */
    for (i = 0, j = properties.length; i < j; i++) {
        if (typeof stage.style[properties[i]] !== 'undefined') {
            prop = properties[i];
            break;
        }
    }

    /* Position video */
    v.style.left = 0;
    v.style.bottom = 0;

    /* If there is a controls element, add the player buttons */
    /* TODO: why does Opera not display the rotation buttons? */
    if (controls) {
        controls.innerHTML = '<button class="play">play</button>' +
            '<div id="change">' +
            '<button class="zoomin">+</button>' +
            '<button class="zoomout">-</button>' +
            '<button class="left">⇠</button>' +
            '<button class="right">⇢</button>' +
            '<button class="up">⇡</button>' +
            '<button class="down">⇣</button>' +
            '<button class="rotateleft">&#x21bb;</button>' +
            '<button class="rotateright">&#x21ba;</button>' +
            '<button class="reset">reset</button>' +
            '</div>';
    }
    let trY;
    let firstPlay = true;


    /* If a button was clicked (uses event delegation)...*/
    controls.addEventListener('click', function(e) {
        t = e.target;
        if (t.nodeName.toLowerCase() === 'button') {

            /* Check the class name of the button and act accordingly */
            switch (t.className) {

                /* Toggle play functionality and button label */
                case 'play':
                    if (v2.paused) {
                        if(firstPlay) {
                            v2.load();
                            v2.addEventListener('canplay', () => {
                                v2.play();
                                t.innerHTML = 'pause';
                                v2.addEventListener('ended', () => {
                                    v2.remove();
                                    v.play();
                                    t.innerHTML = 'pause';
                                });
                            });
                        } else if (document.body.querySelector('#video2')){
                            v2.play();
                            t.innerHTML = 'pause';
                        }

                    } else if (document.body.querySelector('#video2')){
                        firstPlay = false;
                        v2.pause();
                        t.innerHTML = 'play';
                    }

                    if (v.paused && !document.body.querySelector('#video2')) {
                        v.play();
                        t.innerHTML = 'pause';
                    } else if (!document.body.querySelector('#video2')){
                        v.pause();
                        t.innerHTML = 'play';
                    }
                    break;

                    /* Increase zoom and set the transformation */
                case 'zoomin':
                    zoom = (+zoom + 0.1).toFixed(1);


                    switch (+zoom) {
                        case 1.1 :
                            v2.style.bottom = '1.3vh';
                            trY = -4.2;
                            break;
                        case 1.2 :
                            v2.style.bottom = '2.6vh';
                            trY = -7.6;
                            break;
                        case 1.3 :
                            v2.style.bottom = '3.9vh';
                            trY = -9.44;
                            break;
                        case 1.4 :
                            v2.style.bottom = '5.2vh';
                            trY = -11.66;
                            break;
                        case 1.5 :
                            v2.style.bottom = '6.5vh';
                            trY = -13.55;
                            break;
                        case 1.6 :
                            v2.style.bottom = '7.8vh';
                            trY = -15;
                            break;
                    }

                    v.style[prop] = 'scale(' + +zoom + ') rotate(' + rotate + 'deg) translateY(' + trY + 'vh)';
                    v2.style[prop] = 'scale(' + +zoom + ') rotate(' + rotate + 'deg)';
                    break;

                    /* Decrease zoom and set the transformation */
                case 'zoomout':
                    zoom = (+zoom - 0.1).toFixed(1);

                    switch (+zoom) {
                        case 1:
                            v2.style.bottom = '0';
                            trY = 0;
                            break;
                        case 1.1 :
                            v2.style.bottom = '1.3vh';
                            trY = -3.66;
                            break;
                        case 1.2 :
                            v2.style.bottom = '2.6vh';
                            trY = -6.88;
                            break;
                        case 1.3 :
                            v2.style.bottom = '3.9vh';
                            trY = -9.44;
                            break;
                        case 1.4 :
                            v2.style.bottom = '5.2vh';
                            trY = -11.66;
                            break;
                        case 1.5 :
                            v2.style.bottom = '6.5vh';
                            trY = -13.55;
                            break;
                        case 1.6 :
                            v2.style.bottom = '7.8vh';
                            trY = -15;
                            break;
                    }
                    v.style[prop] = 'scale(' + +zoom + ') rotate(' + rotate + 'deg) translateY(' + trY + 'vh)';
                    v2.style[prop] = 'scale(' + +zoom + ') rotate(' + rotate + 'deg)';

                    break;

                    /* Increase rotation and set the transformation */
                case 'rotateleft':
                    rotate = rotate + 5;
                    v.style[prop] = 'rotate(' + rotate + 'deg) scale(' + zoom + ')';
                    break;
                    /* Decrease rotation and set the transformation */
                case 'rotateright':
                    rotate = rotate - 5;
                    v.style[prop] = 'rotate(' + rotate + 'deg) scale(' + zoom + ')';
                    break;

                    /* Move video around by reading its left/top and altering it */
                case 'left':
                    v.style.left = (parseInt(v.style.left, 10) - 5) + 'px';
                    break;
                case 'right':
                    v.style.left = (parseInt(v.style.left, 10) + 5) + 'px';
                    break;
                case 'up':
                    v.style.top = (parseInt(v.style.top, 10) - 5) + 'px';
                    break;
                case 'down':
                    v.style.top = (parseInt(v.style.top, 10) + 5) + 'px';
                    break;

                    /* Reset all to default */
                case 'reset':
                    zoom = 1;
                    rotate = 0;
                    v.style.top = 0 + 'px';
                    v.style.left = 0 + 'px';
                    v.style[prop] = 'rotate(' + rotate + 'deg) scale(' + zoom + ')';
                    break;
            }

            e.preventDefault();
        }
    }, false);

})();


function zooming() {
    var v = document.getElementsByTagName('video')[0];
    var zoom = 1.6;
    var prop = 'transform';
    v.style[prop] = 'scale(' + zoom + ') rotate(' + rotate + 'deg)';
    v.style.top = (parseInt(v.style.top, 10) - 200) + 'px';
}

// setTimeout("document.getElementsByTagName('video')[0].play();", 7000);
// setTimeout("document.getElementById('video2').play();", 1000);
// setTimeout("document.getElementById('video2').style.display = 'none';", 15000);
//
// setTimeout("zooming()", 15000);
